<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group(['prefix' => 'api'], function () {
//    Route::get('/products', function () {
//        return factory('App\Product', 1000)->create();
//    });
    Route::put('/products/activate', 'ProductController@activate');
    Route::put('/products/inactivate', 'ProductController@inactivate');
    Route::put('/products/delete', 'ProductController@deleteAll');
    Route::get('/products/top', 'ProductController@top');
    Route::get('/products/{id}', 'ProductController@get');
    Route::get('/products', 'ProductController@index');
    Route::post('/products', 'ProductController@create');
    Route::put('/products/{id}', 'ProductController@update');
    Route::delete('/products/{id}', 'ProductController@delete');

    Route::get('/categories/{id}', 'CategoryController@get');
    Route::get('/categories', 'CategoryController@index');
    Route::post('/categories', 'CategoryController@create');
    Route::put('/categories/{id}', 'CategoryController@update');
    Route::delete('/categories/{id}', 'CategoryController@delete');

    Route::post('/cart/add', 'CartController@add');
    Route::post('/cart/delete', 'CartController@delete');
    Route::post('/cart/clear', 'CartController@clear');

    Route::get('/orders/{id}', 'OrderController@get');
    Route::get('/orders', 'OrderController@index')->middleware('auth');
    Route::post('/orders', 'OrderController@create');

    Route::post('/feedback', 'SpaController@feedback');
});

Route::get('/config', 'SpaController@config');
Route::get('/{any}', 'SpaController@index')->where('any', '.*');
