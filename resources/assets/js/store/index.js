import Vue from 'vue';
import Vuex from 'vuex';
import cart from './modules/cart';
import products from './modules/products';
import config from './modules/config';
import orders from './modules/orders';
import categories from './modules/categories';
import statuses from './modules/statuses';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        cart,
        products,
        config,
        orders,
        categories,
        statuses,
    }
});
