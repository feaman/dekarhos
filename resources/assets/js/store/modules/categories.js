import shop from '../../api/shop';

// initial state
const state = {
    all: [],
    active: [],
    pagesTotal: 1,
    page: 1,
  categoriesProducts: [],
};

// getters
const getters = {
    getAll: state => state.all,
    getActive: state => state.active,
    pagesTotal: state => state.pagesTotal,
    getPage: state => state.page,
    getCategoriesProducts: state => state.categoriesProducts,
};

// actions
const actions = {
    dispatchAll({commit, state}, payload) {
        payload = payload || {};
        shop.getCategories((categories, lastPage) => {
            commit('setAll', categories);
            commit('setPagesTotal', lastPage);
        }, state.page, payload.perPage ? payload.perPage : 12);
    },
};

// mutations
const mutations = {
    setAll(state, categories) {
        state.all = categories;
    },

  setCategoriesProducts(state, categoriesProducts) {
        state.categoriesProducts = categoriesProducts;
    },

    setActive(state, categories) {
        state.active = categories;
    },

    add(state, category) {
        state.all.push(category);
    },

    deleteCategory(state, category) {
        state.all = state.all.filter(element => element.id !== category.id);
    },

    setPagesTotal(state, lastPage) {
        state.pagesTotal = lastPage;
    },

    setPage(state, page) {
        state.page = page;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
