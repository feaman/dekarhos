// initial state
const state = {
    all: [],
};

// getters
const getters = {
    getAll: state => state.all,
    getIdByName: state => name => {
        return state.all.find(status => status.name === name).id;
    },
};

// mutations
const mutations = {
    setAll(state, statuses) {
        state.all = statuses;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
};
