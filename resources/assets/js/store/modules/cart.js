import shop from '../../api/shop';

// initial state
// shape: [{ id, quantity }]
const state = {
    added: [],
    total: 0,
};

// getters
const getters = {
    cartProducts: state => state.added,

    total: state => state.total,

    isProductAdded: state => product => {
        return !!state.added.find(item => item.id === product.id);
    }
};

// actions
const actions = {
    order({state, commit}, payload) {

        const callback = payload.callback;
        const errorCallback = payload.errorCallback;
        const contacts = payload.contacts;

        shop.createOrder({
            items: state.added,
            contacts,
        }, callback, errorCallback);
    },

    clear({state, commit}) {
        commit('setCartItems', {items: []});
        commit('updateTotal');

        const errorCallback = (error) => {
            alert(error);
        };
        shop.clearCart(errorCallback);
    },

    remove({state, commit}, product) {
        commit('setCartItems', {items: []});
        commit('updateTotal');

        const errorCallback = (error) => {
            alert(error);
        };
        shop.clearCart(errorCallback);
    },

    addProductToCart({state, commit}, product) {
        product.quantity = 1;
        commit('pushProductToCart', product);

        const errorCallback = (error) => {
            alert(error);
        };
        shop.addProductToCart(product, errorCallback);
    },

    removeProductFromCart({state, commit}, product) {
        state.added = state.added.filter(element => element.id !== product.id);

        const errorCallback = (error) => {
            alert(error);
        };
        shop.removeProductFromCart(product, errorCallback);
    },

    setProductQuantity({state, commit}, product) {
        commit('updateTotal');

        const errorCallback = (error) => {
            alert(error);
        };
        shop.addProductToCart(product, errorCallback);
    }
};

// mutations
const mutations = {
    updateTotal: (state) => {
        let total = 0;

        if (state.added) {
            total = state.added.reduce((total, product) => {
                return Math.round((total + product.price * product.quantity) * 100) / 100;
            }, 0);
        }

        state.total = Math.round(total * 100) / 100;
    },
    pushProductToCart(state, product) {
        state.added.push(product);
        this.commit('cart/updateTotal');
    },
    setItemQuantity(state, {id, quantity}) {
        const cartItem = state.added.find(item => item.id === id);
        cartItem.quantity = quantity;
    },
    setCartItems(state, {items}) {
        state.added = items;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
