import shop from '../../api/shop';

// initial state
const state = {
    all: [],
    pagesTotal: 1,
    category: null,
    searchText: '',
    page: 1,
    top: [],
};

// getters
const getters = {
    getAll: state => state.all,
    getPage: state => state.page,
    getActiveCategory: state => state.category,
    pagesTotal: state => state.pagesTotal,
    getSearchText: state => state.searchText,
    getTop: state => state.top,
    getByIds: state => ids => {
        return state.all.filter(product => ids.includes(product.id));
    },
};

// actions
const actions = {
    dispatchAll({commit, state}, payload) {
        const inactive = payload && payload.inactive;
        shop.getProducts((products, lastPage) => {
            commit('setAll', products);
            commit('setPagesTotal', lastPage);
            if (payload && payload.callback) {
                payload.callback();
            }
        }, state.page, state.category ? state.category.id : null, state.searchText, inactive);
    },
    dispatchTop({commit}, callback) {
        shop.getTopProducts((products) => {
            commit('setTop', products);
            callback();
        });
    },
};

// mutations
const mutations = {
    setAll(state, products) {
        state.all = products;
    },

    setTop(state, topProducts) {
        state.top = topProducts;
    },

    setCategory(state, category) {
        state.category = category;
    },

    setPage(state, page) {
        state.page = page;
    },

    setSearchText(state, searchText) {
        state.searchText = searchText;
    },

    addProduct(state, product) {
        state.all.push(product);
    },

    deleteProduct(state, product) {
        state.all = state.all.filter(element => element.id !== product.id);
    },

    decrementProductQuantity(state, {id}) {
        const product = state.all.find(product => product.id === id);
        product.quantity--;
    },

    setPagesTotal(state, lastPage) {
        state.pagesTotal = lastPage;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
