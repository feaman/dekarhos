import shop from '../../api/shop';

// initial state
const state = {
    all: [],
    pagesTotal: 1,
    page: 1,
};

// getters
const getters = {
    allOrders: state => state.all,
    pagesTotal: state => state.pagesTotal,
    getPage: state => state.page,
};

// actions
const actions = {
    dispatchAll({commit, state}) {
        return new Promise((resolve) => {
            shop.getOrders((orders, lastPage) => {
                commit('setOrders', orders);
                commit('setPagesTotal', lastPage);
                resolve();
            }, state.page);
        })
    },
};

// mutations
const mutations = {
    setOrders(state, orders) {
        state.all = orders;
    },

    addOrder(state, order) {
        state.all.push(order);
    },

    deleteOrder(state, order) {
        state.all = state.all.filter(element => element.id !== order.id);
    },

    setPagesTotal(state, lastPage) {
        state.pagesTotal = lastPage;
    },

    setPage(state, page) {
        state.page = page;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
