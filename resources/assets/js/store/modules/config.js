// initial state
const state = {
    user: null,
    ordersCount: 0,
};

// getters
const getters = {
    user: state => state.user,
    getOrdersCount: state => state.ordersCount,
};

// actions
const actions = {
};

// mutations
const mutations = {
    setUser(state, user) {
        state.user = user;
    },
    setOrdersCount(state, ordersCount) {
        state.ordersCount = ordersCount;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
