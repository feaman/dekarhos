import {mapActions} from 'vuex';

export default {
    data() {
        return {
        };
    },
    props: ['product'],
    computed: {
        isProductAdded: component => component.$store.getters['cart/isProductAdded'],
    },
    methods: Object.assign(
        {
            show(product) {
                this.$emit('show', product);
            },
        },
        mapActions('cart', [
            'addProductToCart',
            'removeProductFromCart',
        ]),
    )
};
