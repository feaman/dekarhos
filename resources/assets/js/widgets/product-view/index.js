export default {
    data() {
        return {};
    },
    props: ['product'],
    methods: {
        handleTitle(title) {
            console.log(this.product);
            const searchText = this.$store.getters['products/getSearchText'];
            if (searchText) {
                return title.replace(new RegExp(searchText, 'gi'), '<span class="pink lighten-4">' + searchText + '</span>');
            }

            return title;
        },
    },
};
