import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import './styles/main.styl';
import store from './store';
import axios from 'axios';

Vue.use(Vuetify);
Vue.use(VueRouter);

import App from './components/app/app';
import Products from './components/app/products/list/list';
import PanelPage from './components/panel/panel';
import NotFoundPage from './components/not-found/not-found';
import HomePage from './components/app/home/home';
import CartPage from './components/app/cart/cart';
import PanelProductsPage from './components/panel/products/list/list';
import PanelProductFormPage from './components/panel/products/form/form';
import PanelOrderViewPage from './components/panel/orders/order/order';
import PanelHomePage from './components/panel/home/home';
import AttachWidget from './components/panel/products/attach/attach';
import ContactsPage from './components/app/contacts/contacts';
import FeedbackPage from './components/app/feedback/feedback';
import OrdersPage from './components/panel/orders/list/list';
import ProductCategoriesPage from './components/panel/categories/list/list';
import ProductCardWidget from './widgets/product-card/product-card';
import ProductViewWidget from './widgets/product-view/product-view';

Vue.component('attach', AttachWidget);
Vue.component('product-card', ProductCardWidget);
Vue.component('product-view', ProductViewWidget);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: App,
            children: [
                {
                    path: '/',
                    name: 'home',
                    component: HomePage,
                },
                {
                    path: '/cart',
                    name: 'cart',
                    component: CartPage,
                },
                {
                    path: '/products',
                    name: 'products',
                    component: Products,
                },
                {
                    path: '/feedback',
                    component: FeedbackPage,
                },
                {
                    path: '/contacts',
                    component: ContactsPage,
                },
            ]
        },
        {
            path: '/panel',
            name: 'panel',
            component: PanelPage,
            children: [
                {
                    path: '/panel/',
                    component: PanelHomePage,
                },
                {
                    path: '/panel/product/:id?',
                    component: PanelProductFormPage,
                    props: true,
                },
                {
                    path: '/panel/order/:id?',
                    component: PanelOrderViewPage,
                    props: true,
                },
                {
                    path: '/panel/products',
                    component: PanelProductsPage,
                },
                {
                    path: '/panel/products-inactive',
                    component: PanelProductsPage,
                },
                {
                    path: '/panel/categories',
                    component: ProductCategoriesPage,
                },
                {
                    path: '/panel/orders',
                    component: OrdersPage,
                },
            ]
        },
        {
            path: '*',
            component: NotFoundPage
        }
    ],
});

router.beforeEach((to, from, next) => {
    if (to.fullPath.includes('panel', 1) || to.fullPath.includes('panel', 0)) {
        if (!store.getters['config/user']) {
            return window.location.href = `/login?redirect=${encodeURIComponent(to.fullPath)}`;
        }
    }

    next();
});

axios
    .get('/config')
    .then((response) => {
        store.commit('config/setUser', response.data.user);
        store.commit('config/setOrdersCount', response.data.ordersCount);

        store.commit('cart/setCartItems', {items: response.data.cartProducts});
        store.commit('cart/updateTotal');

        store.commit('categories/setActive', response.data.activeCategories);

        store.commit('statuses/setAll', response.data.statuses);

        new Vue({
            el: '#app',
            store,
            template: '<router-view></router-view>',
            router,
        });
    });
