import api from "../../api/shop";
import store from "../../store";

export default {
    data: () => ({
        drawer: null,
        catalogs: [
            {icon: 'add', text: 'Create label'}
        ],
    }),
    created() {
        if (this.user.is_admin) {
            this.$store.dispatch('categories/dispatchAll');
        }
    },
    computed: {
        user: panel => panel.$store.getters['config/user'],
        currentUser: component => component.$store.getters['config/user'],
        categories: panel => panel.$store.getters['categories/getAll'],
    },
    methods: {
        logout: function() {
            const callback = () => {
                window.location.href = '/login';
            };
            store.commit('config/setUser', null);
            api.logout(callback);
        },
        goCategory(category) {
            this.$store.commit('products/setCategory', category);
            this.$store.dispatch('products/dispatchAll');
            this.$router.push('/panel/products?category_id=' + category.id);
        },
        goToInactive() {
            this.$store.commit('products/setCategory', null);
            this.$store.dispatch('products/dispatchAll', {inactive: 1});
            this.$router.push('/panel/products?inactive=1');
        },
    },
};