import api from "../../../../api/shop";

export default {
    data() {
        return {
            loading: false,
            page: 1,
            currentCategory: {},
            formDialog: false,
            deleteDialog: false,
            errors: [],
            categoryToDelete: {},
        };
    },
    created() {
        if (this.$route.query.page) {
            this.page = parseInt(this.$route.query.page);
        }
    },
    computed: {
        categories: component => component.$store.getters['categories/getAll'],
        pagesTotal: component => component.$store.getters['categories/pagesTotal'],
    },
    watch: {
        page: function(page) {
            this.$store.commit('categories/setPage', page);
            this.$store.dispatch('categories/dispatchAll');
            this.$router.push({query: this.getQueryObject()});
        }
    },
    methods: {
        getQueryObject() {
            let query = {
                page: this.page,
            };

            const category = this.$store.getters['products/getActiveCategory'];
            if (category && category.id) {
                query.category_id = category.id;
            }

            return query;
        },
        editOrCreate(category) {
            if (!category) {
                category = {
                    title: '',
                    url: '',
                    description: '',
                };
            }

            this.currentCategory = category;
            this.formDialog = true;
        },
        save() {
            this.errors = [];

            const errorCallback = errors => {
                if (typeof errors === 'object') {
                    for (const field in errors) {
                        this.errors.push(errors[field]);
                    }
                } else {
                    this.errors.push(errors);
                }
            };

            const callback = (category) => {
                this.errors = [];
                this.currentCategory.file_path = category.file_path;

                if (!this.currentCategory.id) {
                    this.$store.commit('categories/add', category);
                }

                this.formDialog = false;
            };

            api.commitCategory(this.currentCategory, callback, errorCallback);
        },
        remove() {
            const errorCallback = error => {
                alert(error);
            };

            const callback = () => {
                this.$store.commit('categories/deleteCategory', this.categoryToDelete);
            };

            api.deleteCategory(this.categoryToDelete, callback, errorCallback);
            this.deleteDialog = false;
        },
        deleteAlert(category) {
            this.categoryToDelete = category;
            this.deleteDialog = true;
        },
    },
};
