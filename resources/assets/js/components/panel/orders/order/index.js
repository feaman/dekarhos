import api from "../../../../api/shop";

export default {
    data() {
        return {
            loading: true,
            order: null,
        };
    },
    props: ['id'],
    computed: {
        currentUser: component => component.$store.getters['config/user'],
    },
    created() {
        const callback = (order) => {
            this.order = order;
            this.loading = false;
        };
        api.getOrder(this.id, callback);
    },
    methods: {
        getFio(user) {
            if (user) {
                return `${user.first_name} ${user.second_name}`;
            }
            return 'неизвестный НЛО';
        },
        calculateTotal() {
            let total = 0;
            this.order.items.forEach(product => {
                total += Math.round(product.quantity * product.price * 100) / 100;
                return Math.round(total * 100) / 100;
            });
            return total;
        },
    },
};
