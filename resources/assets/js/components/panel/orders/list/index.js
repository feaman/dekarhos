export default {
    data() {

        let headers = [
            {text: '#', value: 'id', align: 'center', class: 'short-th', sortable: false},
            {text: 'Дата', value: 'created', align: 'center', class: 'short-th', sortable: false},
        ];

        if (this.$store.getters['config/user'].is_admin) {
            headers.push({text: 'Пользователь', value: 'user', class: ['hidden-xs-only', 'short-th'], sortable: false});
        }

        headers.push({text: 'Сумма', value: 'sum', align: 'center', class: 'short-th', sortable: false});
        headers.push({text: '', value: 'show', class: 'short-th', sortable: false});

        return {
            loading: true,
            page: 1,
            currentOrder: {},
            dialog: false,
            headers,
        };
    },
    created() {
        this.$store.commit('orders/setOrders', []);
        if (this.$route.query.page) {
            this.page = parseInt(this.$route.query.page);
        }
        this.$store.commit('orders/setPage', 1);
        this.$store.dispatch('orders/dispatchAll').then(() => {
            this.loading = false;
        });
    },
    computed: {
        orders: component => component.$store.getters['orders/allOrders'],
        pagesTotal: component => component.$store.getters['orders/pagesTotal'],
        currentUser: component => component.$store.getters['config/user'],
    },
    watch: {
        page: function (page) {
            this.$store.commit('orders/setPage', page);
            this.$store.dispatch('orders/dispatchAll');
            this.$router.push({query: this.getQueryObject()});
        }
    },
    methods: {
        show(order) {
            this.currentOrder = order;
            this.dialog = true;
        },
        calculateTotal(order) {
            let total = 0;

            if (order) {
                order.items.forEach(product => {
                    total += Math.round(product.quantity * product.price * 100) / 100;
                });
            }

            return Math.round(total * 100) / 100;
        },
        getFio(user) {
            if (user) {
                return `${user.first_name} ${user.second_name}`;
            }
            return 'неизвестный НЛО';
        },
        getQueryObject() {
            let query = {
                page: this.page,
            };

            const category = this.$store.getters['products/getActiveCategory'];
            if (category && category.id) {
                query.category_id = category.id;
            }

            return query;
        },
    },
};
