import api from "../../../../api/shop";

export default {
    data() {
        return {
            loading: false,
            dialog: false,
            deleteDialog: false,
            deleteAllDialog: false,
            page: 1,
            errors: [],
            productToDelete: null,
            inactive: false,
            isSelectAll: false,
            selected: [],
        };
    },
    created() {
        if (this.$route.query.page) {
            this.page = parseInt(this.$route.query.page);
        }
        this.inactive = this.$route.query.inactive;
        this.$store.commit('products/setPage', this.page);
        this.$store.commit('products/setAll', []);
        this.$store.dispatch('products/dispatchAll', {inactive: this.inactive});
        this.$store.dispatch('categories/dispatchAll', this.page);
    },
    beforeUpdate() {
        this.inactive = this.$route.query.inactive;
    },
    mounted() {
        this.isSelectAll = false;
    },
    computed: {
        products: component => component.$store.getters['products/getAll'],
        categories: component => component.$store.getters['categories/getAll'],
        pagesTotal: component => component.$store.getters['products/pagesTotal'],
    },
    watch: {
        page: function(page) {
            this.$store.commit('products/setPage', page);
            this.$store.dispatch('products/dispatchAll');
            this.$router.push({query: this.getQueryObject()});
        }
    },
    methods: {
        isAllSelected() {
            this.isSelectAll = this.selected.length === this.products.length;
        },
        selectAll() {
            if (this.isSelectAll) {
                this.products.forEach(product => this.selected.push(product.id));
            } else {
                this.selected = [];
            }
        },
        remove() {
            const errorCallback = error => {
                alert(error);
            };

            const callback = () => {
                this.$store.commit('products/deleteProduct', this.productToDelete);
            };

            this.deleteDialog = false;
            api.deleteProduct(this.productToDelete, callback, errorCallback);
        },
        activate(product) {
            const errorCallback = error => {
                alert(error);
            };

            product.status_id = this.$store.getters['statuses/getIdByName']('active');
            this.$store.commit('products/deleteProduct', product);
            api.commitProduct(product, null, errorCallback);
        },
        inactivate(product) {
            const errorCallback = error => {
                alert(error);
            };

            product.status_id = this.$store.getters['statuses/getIdByName']('inactive');
            this.$store.commit('products/deleteProduct', product);
            api.commitProduct(product, null, errorCallback);
        },
        deleteSelected() {
            const errorCallback = error => {
                alert(error);
            };
            const products = this.$store.getters['products/getByIds'](this.selected);
            products.forEach(product => {
                product.status_id = this.$store.getters['statuses/getIdByName']('inactive');
                this.$store.commit('products/deleteProduct', product);
            });
            api.deleteProducts(this.selected, errorCallback);
            this.selected = [];
            this.deleteAllDialog = false;
        },
        inactivateSelected() {
            const errorCallback = error => {
                alert(error);
            };
            const products = this.$store.getters['products/getByIds'](this.selected);
            products.forEach(product => {
                product.status_id = this.$store.getters['statuses/getIdByName']('inactive');
                this.$store.commit('products/deleteProduct', product);
            });
            api.inactivateProducts(this.selected, errorCallback);
            this.selected = [];
        },
        activateSelected() {
            const errorCallback = error => {
                alert(error);
            };
            const products = this.$store.getters['products/getByIds'](this.selected);
            products.forEach(product => {
                product.status_id = this.$store.getters['statuses/getIdByName']('inactive');
                this.$store.commit('products/deleteProduct', product);
            });
            api.activateProducts(this.selected, errorCallback);
            this.selected = [];
        },
        isActive(product) {
            return product.status_id === this.$store.getters['statuses/getIdByName']('active');
        },
        getColor(product) {
            return this.selected.includes(product.id) ? 'purple lighten-4' : null;
        },
        getQueryObject() {
            let query = {
                page: this.page,
            };

            const category = this.$store.getters['products/getActiveCategory'];
            if (category && category.id) {
                query.category_id = category.id;
            }

            if (this.inactive) {
                query.inactive = 1;
            }

            return query;
        },
        deleteAlert(product) {
            this.productToDelete = product;
            this.deleteDialog = true;
        },
        deleteAllAlert() {
            this.deleteAllDialog = true;
        },
    },
};
