import api from "../../../../api/shop";

export default {
    props: ['id'],
    data() {
        return {
            title: '',
            price: '',
            category_id: '',
            description: '',
            file: {file_path: '/pictures/no_image.png'},
            loading: true,
            errors: [],
        };
    },
    created() {
        if (this.id) {
            // Получение товара по идентификатору
            const callback = (product) => {
                this.title = product.title;
                this.price = product.price;
                this.category_id = product.category_id;
                this.description = product.description;
                this.loading = false;

                if (product.file) {
                    this.file.file_path = `/files/${product.file.name}_thumbs.${product.file.extension}`;
                }
            };
            api.getProduct(this.id, callback);
        } else {
            this.loading = false;
        }

        const currentCategory = this.$store.getters['products/getActiveCategory'];
        if (currentCategory) {
            this.category_id = currentCategory.id;
        }
    },
    computed: {
        products: component => component.$store.getters['products/getAll'],
        categories: component => component.$store.getters['categories/getAll'],
        errorsView: function() {
            return this.errors.join('<br>');
        },
    },
    methods: {
        save() {
            this.errors = [];
            const product = {
                id: this.id,
                title: this.title,
                price: this.price,
                category_id: this.category_id,
                description: this.description,
                file_info: this.file.fileInfo,
            };

            const errorCallback = errors => {
                if (typeof errors === 'object') {
                    for (const field in errors) {
                        this.errors.push(errors[field]);
                    }
                } else {
                    this.errors.push(errors);
                }
            };

            const callback = () => {
                this.errors = [];
                this.$router.push('/panel/products');
            };

            api.commitProduct(product, callback, errorCallback);
        },
        remove(product) {
            const errorCallback = error => {
                alert(error);
            };

            const callback = () => {
                this.$store.commit('products/deleteProduct', product);
            };

            api.deleteProduct(product, callback, errorCallback);
        },
    },
};
