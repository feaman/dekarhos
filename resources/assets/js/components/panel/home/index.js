export default {
    data() {
        return {};
    },
    computed: {
        currentUser: component => component.$store.getters['config/user'],
        ordersCount: component => component.$store.getters['config/getOrdersCount'],
    },
};