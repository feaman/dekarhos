import api from "../../../../api/shop";

export default {
    data() {
        return {
            dialog: false,
            loading: true,
            error: null,
            page: 1,
            currentProduct: null,
        };
    },
    created() {
        this.$store.commit('products/setAll', []);
        this.$store.commit('products/setPage', 1);
        if (this.$route.query.page) {
            this.page = parseInt(this.$route.query.page);
        }

        const productsCallback = () => {
            this.loading = false;
        };
        if (this.$route.query.category_id) {
            const callback = (category) => {
                this.$store.commit('products/setCategory', {
                    id: this.$route.query.category_id,
                    title: category.title,
                });
                this.$store.dispatch('products/dispatchAll', {callback: productsCallback});
            };

            api.getCategory(this.$route.query.category_id, callback);
        } else {
            this.$store.dispatch('products/dispatchAll', {callback: productsCallback});
        }
        this.$store.commit('products/setSearchText', '');
    },
    computed: {
        currentCategory: component => component.$store.getters['products/getActiveCategory'],
        products: component => component.$store.getters['products/getAll'],
        pagesTotal: component => component.$store.getters['products/pagesTotal'],
    },
    watch: {
        page: function (page) {
            this.$store.commit('products/setPage', page);
            this.$store.dispatch('products/dispatchAll');
            this.$router.push({query: this.getQueryObject()});
        }
    },
    methods:
        {
            show(product) {
                this.currentProduct = product;
                this.dialog = true;
            },
            getQueryObject() {
                let query = {
                    page: this.page,
                };

                const searchText = this.$store.getters['products/getSearchText'];
                if (searchText) {
                    query.search = searchText;
                }

                const category = this.$store.getters['products/getActiveCategory'];
                if (category && category.id) {
                    query.category_id = category.id;
                }

                return query;
            },
        },
};
