import {mapActions} from 'vuex';

export default {
    data() {
        return {
            dialog: false,
            currentProduct: null,
            loading: true,
        };
    },
    created() {
        this.$store.dispatch('products/dispatchTop', () => { this.loading = false; });
    },
    computed: {
        topProducts: component => component.$store.getters['products/getTop'],
        isProductAdded: component => component.$store.getters['cart/isProductAdded'],
    },
    methods:
        Object.assign(
            {
                show(product) {
                    this.currentProduct = product;
                    this.dialog = true;
                },
                show(product) {
                    this.currentProduct = product;
                    this.dialog = true;
                },
            },
            mapActions('cart', [
                'addProductToCart',
                'removeProductFromCart',
            ]),
        )
};