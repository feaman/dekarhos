export default {
    data() {
        return {
            headers: [
                {text: '', value: 'file_path', sortable: false,},
                {text: 'Название', value: 'title', sortable: false,},
                {text: 'Цена', value: 'price', sortable: false,},
                {text: 'Количество', value: 'quantity', sortable: false,},
            ],
            success: false,
            loading: false,
            dialog: false,
            contacts: {},
            email: '',
            phone: '',
            errors: [],
            rules: {
                required: (value) => !!value || 'Обязательное',
                email: (value) => {
                    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return pattern.test(value) || 'Неверный формат электропочты';
                },
                phone: (value) => {
                    const pattern = /^[+0-9-\(\)\s]+$/;
                    return pattern.test(value) || 'Неверный формат телефона';
                },
            }
        };
    },
    mounted() {
        if (this.currentUser) {
            this.email = this.currentUser.email;
            this.phone = this.currentUser.phone;
        }
    },
    computed: {
        currentUser: component => component.$store.getters['config/user'],
        products: component => component.$store.getters['cart/cartProducts'],
        total: component => component.$store.getters['cart/total'],
        errorsView: function() {
            return this.errors.join('<br>');
        },
    },
    methods: {
        setProductQuantity(product) {
            const filteredValue = product.quantity.toString().replace(/[^0-9\.]/g, '');
            product.quantity = filteredValue;
            this.$store.dispatch('cart/setProductQuantity', product);
        },
        increment(product) {
            product.quantity++;
            this.setProductQuantity(product);
        },
        decrement(product) {
            if (product.quantity > 1) {
                product.quantity--;
                this.setProductQuantity(product);
            }
        },
        order() {
            this.loading = true;
            this.errors = [];

            const errorCallback = errors => {
                this.loading = false;
                if (typeof errors === 'object') {
                    for (const field in errors) {
                        this.errors.push(errors[field]);
                    }
                } else {
                    this.errors.push(errors);
                }
            };

            const callback = () => {
                this.loading = false;
                this.success = true;
                this.$store.commit('cart/setCartItems', {items: []});
                this.$store.commit('config/setOrdersCount', this.$store.getters['config/getOrdersCount'] + 1);
            };

            this.$store.dispatch('cart/order', {callback, errorCallback, contacts: {email: this.email, phone: this.phone}});
        },
        clear() {
            this.$store.dispatch('cart/clear');
        },
        remove(product) {
            this.$store.dispatch('cart/removeProductFromCart', product);
        },
        getContacts() {
            this.dialog = true;
        },
    }
};