import api from "../../../api/shop";

export default {
    props: ['id'],
    data() {
        return {
            email: '',
            phone: '',
            message: '',
            loading: false,
            errors: [],
            success: false,
        };
    },
    computed: {
        errorsView: function() {
            return this.errors.join('<br>');
        },
    },
    methods: {
        send() {
            this.loading = true;
            this.errors = [];
            this.success = false;
            const data = {
                email: this.email,
                phone: this.phone,
                message: this.message,
            };

            const errorCallback = errors => {
                this.loading = false;
                if (typeof errors === 'object') {
                    for (const field in errors) {
                        this.errors.push(errors[field]);
                    }
                } else {
                    this.errors.push(errors);
                }
            };

            const callback = () => {
                this.errors = [];
                this.loading = false;
                this.success = true;
                this.email = '';
                this.phone = '';
                this.message = '';
            };

            api.sendFeedback(data, callback, errorCallback);
        },
    },
};
