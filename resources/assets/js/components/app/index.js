import api from "../../api/shop";

export default {
    data: () => ({
        drawer: null,
    }),
    computed: {
        categories: component => component.$store.getters['categories/getActive'],
        currentUser: component => component.$store.getters['config/user'],
        cartProductsAmount: component => component.$store.getters['cart/cartProducts'].length,
    },
    methods: {
        getMenuItemClass(url, strict) {
            if ((!strict && this.$route.fullPath.includes(url, 0)) || (strict && this.$route.fullPath === url)) {
                return 'active';
            }
            return '';
        },
        getMenuListItemClass(category) {
            if (this.$route.fullPath.includes(`category_id=${category.id}`)) {
                return 'active';
            }
            return '';
        },
        search(searchText) {
            this.$store.commit('products/setPage', 1);
            this.$store.commit('products/setSearchText', searchText);
            this.$store.dispatch('products/dispatchAll');
        },
        goCategory(category) {
            this.$store.commit('products/setCategory', category);
            this.$store.dispatch('products/dispatchAll');
            this.$router.push('/products?category_id=' + category.id);
        },
        lkAction: function() {
            if (this.$store.getters['config/user']) {
                const callback = () => {
                    window.location.href = '/login';
                };
                this.$store.commit('config/setUser', null);
                api.logout(callback);
            } else {
                window.location.href = '/login';
            }
        },
    },
};