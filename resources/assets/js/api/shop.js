import axios from 'axios';

export default {
    getProducts(callback, page, categoryId, searchText, inactive) {
        let url = `/api/products?page=${(page ? page : 1)}`;

        if (categoryId) {
            url += '&category_id=' + categoryId;
        }

        if (inactive) {
            url += `&inactive=${inactive}`;
        }

        if (searchText) {
            url += '&search=' + searchText;
        }

        axios
            .get(url)
            .then(response => {
                callback(response.data.data, response.data.last_page);
            });
    },
    getProduct(id, callback) {
        axios
            .get('/api/products/' + id)
            .then(response => {
                callback(response.data);
            })
            .catch(error => {
                alert(error.response.data.message || error.message);
            });
    },
    getOrder(id, callback) {
        axios
            .get('/api/orders/' + id)
            .then(response => {
                callback(response.data);
            })
            .catch(error => {
                alert(error.response.data.message || error.message);
            });
    },
    getCategory(id, callback) {
        axios
            .get('/api/categories/' + id)
            .then(response => {
                callback(response.data);
            })
            .catch(error => {
                alert(error.response.data.message || error.message);
            });
    },
    inactivateProducts(ids, errorCallback) {
        axios
            .put('/api/products/inactivate', {ids})
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    activateProducts(ids, errorCallback) {
        axios
            .put('/api/products/activate', {ids})
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    deleteProducts(ids, errorCallback) {
        axios
            .put('/api/products/delete', {ids})
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    getTopProducts(callback) {
        axios
            .get('/api/products/top')
            .then(response => {
                callback(response.data);
            })
            .catch(error => {
                alert(error.response.data.message || error.message);
            });
    },
    getCategories(callback, page, perPage) {
        axios
            .get('/api/categories?page=' + (page ? page : 1) + '&per_page=' + (perPage ? perPage : 12))
            .then(response => {
                callback(response.data.data, response.data.last_page);
            });
    },
    createOrder(params, callback, errorCallback) {
        axios
            .post('/api/orders', {items: params.items, contacts: params.contacts})
            .then(() => {
                callback();
            })
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    getOrders(callback, page) {
        axios
            .get('/api/orders?page=' + (page ? page : 1))
            .then(response => {
                callback(response.data.data, response.data.last_page);
            });
    },
    logout(callback) {
        axios
            .post('/logout')
            .then(() => {
                callback();
            });
    },
    commitProduct(product, callback, errorCallback) {
        let query = null;

        if (product.id) {
            query = axios.put('/api/products/' + product.id, product);
        } else {
            query = axios.post('/api/products', product);
        }

        query
            .then((response) => {
                if (callback) {
                    callback(response.data);
                }
            })
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    sendFeedback(data, callback, errorCallback) {
        axios.post('/api/feedback', data)
            .then((response) => {
                callback(response.data);
            })
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    commitCategory(category, callback, errorCallback) {
        let query = null;

        if (category.id) {
            query = axios.put('/api/categories/' + category.id, category);
        } else {
            query = axios.post('/api/categories', category);
        }

        query
            .then((response) => {
                callback(response.data);
            })
            .catch(error => {
                errorCallback(error.response.data.errors || error.response.data.message);
            });
    },
    deleteProduct(product, callback, errorCallback) {
        axios
            .delete('/api/products/' + product.id)
            .then((response) => {
                callback(response.data);
            })
            .catch(error => {
                errorCallback(error.response.data.message || error.message);
            });
    },
    deleteCategory (category , callback, errorCallback) {
        axios
            .delete('/api/categories/' + category.id)
            .then((response) => {
                callback(response.data);
            })
            .catch(error => {
                errorCallback(error.response.data.message || error.message);
            });
    },
    addProductToCart(product, errorCallback) {
        axios
            .post('/api/cart/add', {product})
            .catch(error => {
                errorCallback(error.response.data.message || error.message);
            });
    },
    removeProductFromCart(product, errorCallback) {
        axios
            .post('/api/cart/delete', {product})
            .catch(error => {
                errorCallback(error.response.data.message || error.message);
            });
    },
    clearCart(errorCallback) {
        axios
            .post('/api/cart/clear')
            .catch(error => {
                errorCallback(error.response.data.message || error.message);
            });
    },
};
