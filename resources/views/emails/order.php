<div>
  <h1>
    <strong>Новый заказ, шеф!</strong>
  </h1>
</div>

<br>

<div>
  Гляди-ка: <a href="<?= $url ?>"><?= $url ?></a>
</div>

<div>
  Позиций: <?= count($order->items()) ?>
</div>

<div>
  Сумма: <strong><?= $sum ?></strong>
</div>

<br>

<div>
  <a href="mailto:<?= $order->email ?>"><?= $order->email ?></a>
</div>

<div>
  <a href="tel:<?= $order->phone ?>"><?= $order->phone ?></a>
</div>
