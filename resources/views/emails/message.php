<span style="font-weight:bold">От:</span> <a href="mailto:<?= $email ?>"><?= $email ?></a>

<?php if ($phone) { ?>
  <br>
  <span style="font-weight:bold">Телефон:</span> <a href="tel:<?= $phone ?>"><?= $phone ?></a>
<?php } ?>

<br>
<span style="font-weight:bold">Сообщение:
<br>
</span> <?= nl2br($text) ?>
