<?php

namespace App\Http\Controllers;

use App\Category;
use App\File;
use App\Product;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $activeStatusId = Status::where('name', '=', Status::ACTIVE)->first()['id'];
        $inactiveStatusId = Status::where('name', '=', Status::INACTIVE)->first()['id'];
        $perPage = $request->input('perPage');
        $product = new Product();
        $builder = $product
            ->newQuery()
            ->select('products.*')
            ->with(['file']);

        if ($searchText = $request->input('search')) {
            $builder->where('products.title', 'like', '%' . $searchText . '%');
        }

        if (($inactive = (int)$request->input('inactive')) && $this->checkAccess()) {
            $builder->where('products.status_id', '=', $inactiveStatusId);
        } else {
            $builder->where('products.status_id', '=', $activeStatusId);
        }

        if ($categoryId = $request->input('category_id')) {
            $builder
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->where('categories.status_id', '=', $activeStatusId)
                ->where('category_id', '=', $categoryId);
        }

        if ($inactive && $this->checkAccess()) {
            return response(['data' => $builder->get()]);
        } else {
            return response($builder->paginate($perPage ?: 12));
        }
    }

    public function get(Request $request, $id)
    {
        $this->checkAccess();

        $product = (new Product())
            ->select('products.*')
            ->join('statuses', 'products.status_id', '=', 'statuses.id')
            ->where('statuses.name', '=', Status::ACTIVE)
            ->with(['file'])
            ->find($id);
        if (!$product) {
            throw new \Exception('Товар #' . $id . ' не найден');
        }

        return response($product);
    }

    public function activate(Request $request)
    {
        $this->checkAccess();
        $activeStatusId = Status::where('name', '=', Status::ACTIVE)->first()['id'];

        if ((!$productIds = $request->input('ids')) || !is_array($productIds)) {
            throw new \Exception('Не переданы идентификаторы товаров');
        }

        DB::table('products')
            ->whereIn('id', $productIds)
            ->update(['status_id' => $activeStatusId]);
    }

    public function inactivate(Request $request)
    {
        $this->checkAccess();
        $inactiveStatusId = Status::where('name', '=', Status::INACTIVE)->first()['id'];

        if ((!$productIds = $request->input('ids')) || !is_array($productIds)) {
            throw new \Exception('Не переданы идентификаторы товаров');
        }

        DB::table('products')
            ->whereIn('id', $productIds)
            ->update(['status_id' => $inactiveStatusId]);
    }

    public function deleteAll(Request $request)
    {
        $this->checkAccess();
        $deletedStatusId = Status::where('name', '=', Status::DELETED)->first()['id'];

        if ((!$productIds = $request->input('ids')) || !is_array($productIds)) {
            throw new \Exception('Не переданы идентификаторы товаров');
        }

        DB::table('products')
            ->whereIn('id', $productIds)
            ->update(['status_id' => $deletedStatusId]);
    }

    public function create(Request $request)
    {
        return $this->save($request);
    }

    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    public function top()
    {
        $result = [];
        $categories = Category::where('statuses.name', '=', Status::ACTIVE)
            ->select('categories.*')
            ->join('statuses', 'categories.status_id', '=', 'statuses.id')
            ->get();
        foreach ($categories as $category) {
            $products = Product::where('category_id', '=', $category->id)
                ->select('products.*')
                ->join('statuses', 'products.status_id', '=', 'statuses.id')
                ->where('statuses.name', '=', Status::ACTIVE)
                ->with(['file'])
                ->limit(4)->get();
            $categoryProducts = $products;
            if ($categoryProducts->count()) {
                $result[] = [
                    'id' => $category->id,
                    'title' => $category->title,
                    'products' => $categoryProducts,
                ];
            }
        }
        return $result;
    }

    public function save(Request $request, $id = null)
    {
        $this->checkAccess();

        $rules = [
            'title' => 'required|string',
            'price' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
            'description' => 'string',
        ];
        if (!$id) {
            $rules['title'] .= '|unique:products';
        }
        $request->validate($rules);

        $productData = $request->post();
        $fileInfo = Arr::get($productData, 'file_info');
        if ($fileData = Arr::get($fileInfo, 'fileData')) {
            $fileData = explode(',', $fileData)[1];
            $file = new File();
            $file->fill([
                'name' => uniqid(),
                'title' => $fileInfo['fileName'],
                'extension' => \File::extension($fileInfo['fileName']),
            ]);
            $file->saveOrFail();
            $productData['file_id'] = $file->id;

            $filePath = public_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $file->name . '.' . $file->extension;
            file_put_contents(
                $filePath,
                base64_decode($fileData)
            );
            $image = Image::make($filePath);
            $image
                ->resize(900, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->save(static::getFilePath($file->name . '_normal', $file->extension));
            $image
                ->resize(null, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->save(static::getFilePath($file->name . '_card', $file->extension));
            $image
                ->crop(200, 200)
                ->save(static::getFilePath($file->name . '_thumbs', $file->extension));
        }

        if ($id) {
            $product = Product::findOrFail($id);
            $product->update($productData);
        } else {
            $product = new Product();
            $product->fill($productData);
            $product->saveOrFail();
        }


        return response($product);
    }

    protected static function getFilePath(string $fileName, string $fileExtension)
    {
        return public_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $fileName . '.' . $fileExtension;
    }

    public function delete($id)
    {
        $this->checkAccess();
        $deletedStatus = Status::where('name', '=', Status::DELETED)->first();
        $product = Product::where('status_id', '<>', $deletedStatus['id'])->findOrFail($id);
        $product->status_id = $deletedStatus['id'];
        $product->saveOrFail();
    }

    private function checkAccess($boolean = false)
    {
        $currentUser = \Auth::user();
        if (!$currentUser || !$currentUser->is_admin) {
            if ($boolean) {
                return false;
            } else {
                throw new \Exception('Доступ запрещён');
            }
        }
        return true;
    }
}
