<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Mail\Message;
use App\Services\TelegramService;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $currentUser = Auth::user();
        $perPage = $request->input('perPage');
        /** @var Builder $orders */
        $orders = Order::with(['user', 'items']);
        $orders->orderBy('id', 'desc');

        if (!$currentUser->is_admin) {
            $orders->where('user_id', '=', $currentUser->id);
        }

        return response($orders->paginate($perPage ?: 12));
    }

    public function count(Request $request)
    {
        $this->checkAccess();
        $currentUser = Auth::user();
        $perPage = $request->input('perPage');
        $orders = Order::orderBy('id', 'desc');

        if (!$currentUser->is_admin) {
            $orders->where('user_id', '=', $currentUser->id);
        }

        return response($orders->paginate($perPage ?: 12));
    }

    public function create(Request $request)
    {
        $request->validate([
            'items' => 'required|array',
            'contacts.email' => 'required|string|email',
            'contacts.phone' => 'required|regex:/^[+0-9-\(\)\s]+$/',
        ]);

        $order = new Order();
        $order->fill([
            'email' => $request->post('contacts')['email'],
            'phone' => $request->post('contacts')['phone'],
        ]);

        if ($currentUser = \Auth::user()) {
            $order->user_id = $currentUser->id;
        }

        DB::beginTransaction();
        try {
            $order->saveOrFail();
            $sum = 0;

            foreach ($request->post('items') as $item) {
                $product = Product::findOrFail($item['id']);
                $orderItem = new OrderItem();
                $orderItem->title = $product->title;
                $orderItem->description = $product->description;
                $orderItem->quantity = $item['quantity'];
                $orderItem->price = $product->price;
                $orderItem->file_path = $product->getFilePath();
                $orderItem->category_title = $product->category->title;
                $orderItem->product_id = $product->id;
                $orderItem->order_id = $order->id;
                $orderItem->saveOrFail();

                $sum += round($orderItem->quantity * $orderItem->price, 2);
            }

            session(['cartProducts' => []]);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

//        try {
//            TelegramService::sendMessage(TelegramService::getNewOrderMessage($order, $sum), ['parse_mode' => 'Markdown']);
//        } catch (\Exception $exception) {}

        try {
            $data = [
                'sum' => $sum,
                'order' => $order,
                'url' => config('app.url') . '/panel/order/' . $order->id,
            ];
            Mail::send('emails.order', $data, function (Message $message) {
                $message
                    ->to(['elf.feaman@gmail.com', 'idekarhos@gmail.com'])
                    ->subject('Новый заказ, шеф!');
            });
        } catch (\Exception $exception) {}

        return response($order);
    }

    public function get(Request $request, $id)
    {
        $this->checkAccess(true);

        if (!$order = Order::with(['user', 'items'])->find($id)) {
            throw new \Exception('Заказ #' . $id . ' не найден');
        }

        return response($order);
    }

    private function checkAccess(bool $isAdmin = false)
    {
        $currentUser = \Auth::user();
        if (!$currentUser || ($isAdmin && !$currentUser->is_admin)) {
            throw new \Exception('Доступ запрещён');
        }
    }
}
