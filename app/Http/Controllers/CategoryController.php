<?php

namespace App\Http\Controllers;

use App\Category;
use App\Status;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $this->checkAccess();

        $categoryModel = new Category();
        $perPage = $request->input('per_page');
        $categories = $categoryModel
            ->select('categories.*')
            ->join('statuses', 'categories.status_id', '=', 'statuses.id')
            ->where('statuses.name', '=', Status::ACTIVE)
            ->paginate($perPage ?: 12);
        return response($categories);
    }

    public function get(Request $request, $id)
    {
        $category = (new Category())
            ->select('categories.*')
            ->join('statuses', 'categories.status_id', '=', 'statuses.id')
            ->where('statuses.name', '=', Status::ACTIVE)
            ->find($id);
        if (!$category) {
            throw new \Exception('Категория #' . $id . ' не найдена');
        }

        return response($category->find($id));
    }

    public function create(Request $request)
    {
        return $this->save($request);
    }

    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    public function save(Request $request, $id = null)
    {
        $this->checkAccess();

        $request->validate([
            'title' => 'required|unique:categories|string',
        ]);

        $categoryData = $request->post();

        if ($id) {
            $category = Category::findOrFail($id);
            $category->update($categoryData);
        } else {
            $category = new Category();
            $category->fill($categoryData);
            $category->saveOrFail();
        }

        return response($category);
    }

    public function delete($id)
    {
        $this->checkAccess();

        $deletedStatus = Status::where('name', '=', Status::DELETED)->first();
        $category = Category::where('status_id', '<>', $deletedStatus['id'])->findOrFail($id);
        $category->status_id = $deletedStatus['id'];
        $category->saveOrFail();

        $categoryProducts = $category->products;
        foreach ($categoryProducts as $categoryProduct) {
            $categoryProduct->status_id = $deletedStatus['id'];
            $categoryProduct->saveOrFail();
        }
    }

    private function checkAccess()
    {
        $currentUser = \Auth::user();
        if (!$currentUser || !$currentUser->is_admin) {
            throw new \Exception('Доступ запрещён');
        }
    }
}
