<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if ($request->query('redirect')) {
            session(['redirect' => $request->query('redirect')]);
        }
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo()
    {
        if (session('redirect')) {
            $redirectTo = session('redirect');
            session(['redirect' => null]);
            return $redirectTo;
        } else {
            return $this->redirectTo;
        }
    }
}
