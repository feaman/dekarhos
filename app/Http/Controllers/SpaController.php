<?php

namespace App\Http\Controllers;

use App\Order;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SpaController extends Controller
{
    public function index()
    {
        return view('spa');
    }

    public function config()
    {
        $ordersCount = 0;
        if ($currentUser = Auth::user()) {
            $orderQuery = (new Order())->newQuery();
            if ($currentUser->is_admin) {
                $ordersCount = $orderQuery->count();
            } else {
                $ordersCount = $orderQuery->where('user_id', '=', $currentUser->id)->count();
            }
        }

        $activeStatusId = Status::where('name', '=', Status::ACTIVE)->first()['id'];
        return response([
            'user' => $currentUser,
            'cartProducts' => session('cartProducts') ?: [],
            'ordersCount' => $ordersCount,
            'statuses' => Status::get(),
            'activeCategories' => DB::table('products')
                ->select('categories.id', 'categories.title')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->where([['products.status_id', '=', $activeStatusId], ['categories.status_id', '=', $activeStatusId]])
                ->groupBy(['categories.id', 'categories.title'])
                ->get(),
        ]);
    }

    public function feedback(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'phone' => 'nullable|regex:/^[+0-9-\(\)\s]+$/',
            'message' => 'required|string',
        ]);
        $data = $request->post();
        $data['text'] = $data['message'];

        Mail::send('emails.message', $data, function (Message $message) {
            $message
                ->to(['elf.feaman@gmail.com', 'nuts.sale@gmail.com'])
                ->subject('Сообщенка из магазинчика');
        });
    }
}
