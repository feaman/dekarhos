<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CartController extends Controller
{
    public function add(Request $request)
    {
        $productData = $request->post('product');
        $quantity = Arr::get($productData, 'quantity', 0);
        $product = Product::findOrFail($productData['id']);
        $productFound = false;

        if ($cartProducts = session('cartProducts')) {
            foreach ($cartProducts as $index => $cartProduct) {
                if ($cartProduct['id'] == $product->id) {
                    session(['cartProducts.' . $index . '.' . 'quantity' => $quantity]);
                    $productFound = true;
                    break;
                }
            }
            if (!$productFound) {
                $cartProducts[] = [
                    'id' => $product->id,
                    'title' => $product->title,
                    'price' => $product->price,
                    'file_path' => $product->file_path,
                    'quantity' => $quantity,
                ];
                session(['cartProducts' => $cartProducts]);
            }
        } else {
            $cartProducts = [
                [
                    'id' => $product->id,
                    'title' => $product->title,
                    'price' => $product->price,
                    'file_path' => $product->file_path,
                    'quantity' => $quantity,
                ],
            ];
            session(['cartProducts' => $cartProducts]);
        }
    }

    public function delete(Request $request)
    {
        $productData = $request->post('product');
        Product::findOrFail($productData['id']);
        $cartProducts = session('cartProducts');
        $newCartProduct = [];

        foreach ($cartProducts as $cartProduct) {
            if ($cartProduct['id'] != $productData['id']) {
                $newCartProduct[] = $cartProduct;
            }
        }

        session(['cartProducts' => $newCartProduct]);
    }

    public function clear(Request $request)
    {
        session(['cartProducts' => []]);
    }
}
