<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'title',
        'file_path',
        'quantity',
        'description',
        'price',
        'order_id',
        'product_id',
    ];

    protected $visible = [
        'id',
        'title',
        'file_path',
        'quantity',
        'description',
        'price',
        'order_id',
        'order',
        'product_id',
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
