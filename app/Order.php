<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'email',
        'phone',
        'user_id',
    ];

    protected $visible = [
        'id',
        'user_id',
        'created',
        'fio',
        'phone',
        'email',
        'user',
        'items',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }

    protected $appends = ['created'];

    public function getCreatedAttribute()
    {
        return date('d.m.Y H:i:s', strtotime($this->created_at));
    }
}
