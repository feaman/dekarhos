<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const DELETED = 'deleted';

    protected $fillable = [
        'name',
        'title',
    ];

    protected $visible = [
        'id',
        'name',
        'title',
    ];

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
