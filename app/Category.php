<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title',
    ];

    protected $visible = [
        'id',
        'title',
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
