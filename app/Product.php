<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title',
        'file_id',
        'description',
        'price',
        'category_id',
        'status_id',
    ];

    protected $visible = [
        'id',
        'title',
        'file_id',
        'file',
        'description',
        'price',
        'category_id',
        'status_id',
    ];

    public function file()
    {
        return $this->belongsTo('App\File');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function getFilePath()
    {
        if ($this->file) {
            return '/files/' . $this->file->name . '.' . $this->file->extension;
        }

        return null;
    }
}
