<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailResetPasswordToken extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     * @return array
     * @internal param mixed $notifiable
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     * @return MailMessage
     * @internal param mixed $notifiable
     */
    public function toMail()
    {
        return (new MailMessage)
            ->subject('Сброс пароля на ' . config('app.url'))
            ->line('Вы получили это письмо потомучто запросили сброс пароля. Для сброса пароля нажмите кнопку ниже:')
            ->action('Сбросить пароль', url('password/reset', $this->token))
            ->line('Если вы не запрашивали сброс пароля - никаких дальнейших действий не требуется. Спасибо, что Вы с нами!');
    }

}