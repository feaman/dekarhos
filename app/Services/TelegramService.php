<?php

namespace App\Services;

use App\Order;
use Zelenin\Telegram\Bot\ApiFactory;

class TelegramService extends BaseService
{
    public static function sendMessage(string $message, array $parameters = [])
    {
        if (!empty(config('app.telegramBot.token')) && !empty(config('app.telegramBot.chatId'))) {
            $api = ApiFactory::create(config('app.telegramBot.token'));

            $parameters = array_merge(
                $parameters,
                [
                    'chat_id' => config('app.telegramBot.chatId'),
                    'text' => $message,
                ]
            );

            $api->sendMessage($parameters);
        }
    }

    public static function getNewOrderMessage(Order $order, float $sum)
    {
        return '*Новый заказ, шеф!*' . ' ' . hex2bin('F09F9883') . PHP_EOL .
            'Гляди-ка: ' . config('app.url') . '/panel/order/' . $order->id . PHP_EOL . PHP_EOL .
            'Позиций: ' . count($order->items) . PHP_EOL .
            'Сумма: *' . $sum . '*' . PHP_EOL . PHP_EOL .
            hex2bin('E29C89') . ' ' . $order->email . PHP_EOL .
            hex2bin('F09F939E') . ' ' . $order->phone;
    }
}