<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('quantity')->unsigned();
            $table->decimal('price', 19, 2);
            $table->string('file_path')->nullable();
            $table->string('category_title');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->text('description');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('email');
            $table->string('phone');
            $table->dropColumn('items');
            $table->dropColumn('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->longText('items');
            $table->longText('contacts');
        });
    }
}
