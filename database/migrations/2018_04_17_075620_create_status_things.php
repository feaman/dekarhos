<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusThings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('name');
            $table->timestamps();
        });
        DB::table('statuses')->insert(
            [
                [
                    'title' => 'Активный',
                    'name' => 'active',
                ],
                [
                    'title' => 'Неактивный',
                    'name' => 'inactive',
                ],
                [
                    'title' => 'Удаленный',
                    'name' => 'deleted',
                ],
            ]
        );

        Schema::table('categories', function (Blueprint $table) {
            $table->integer('status_id')->unsigned()->default(1);
            $table
                ->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->integer('status_id')->unsigned()->default(1);
            $table
                ->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('status_id');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('status_id');
        });
    }
}
